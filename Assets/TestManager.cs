using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class TestManager : MonoBehaviour
{
    public bool UseCachedTransform { get; private set; }

    [SerializeField] TestObject _cubePrefab;
    [SerializeField] GameObject _statsContainer;
    [SerializeField] TMP_Text 
        _useCachedText, 
        _toggleCachedButtonText, 
        _iterationsText, 
        _cachedFpsText, 
        _cachedMsText, 
        _uncachedFpsText, 
        _uncachedMsText, 
        _frameTimeDiffText;
    [SerializeField] Slider _iterationsSlider;

    List<TestObject> _testObjects;

    float[] _timeArray;
    float[] _fpsArray;
    float _deltaTime, _totalTime, _totalFps, _fpsDeltaTime;
    int _counter;
    int _cachedFps, _cachedMs, _uncachedFps, _uncachedMs;
    int _iterations;

    void OnEnable()
    {
        _testObjects = new List<TestObject>();
        for (int i = 0; i < _iterationsSlider.maxValue; ++i)
        {
            _testObjects.Add(Instantiate(_cubePrefab, Vector3.zero, Quaternion.identity));
        }

        _timeArray = new float[120];
        _fpsArray = new float[120];
        _iterationsText.text = $"{_iterationsSlider.value}";
        _iterations = (int)_iterationsSlider.value;
        _iterationsSlider.onValueChanged.AddListener(IterationsSliderChanged);
    }

    void Update()
    {
        if (UseCachedTransform)
        {
            UpdateTestObjectsUsingCachedTransform();
        }
        else
        {
            UpdateTextObjectsUsingUncachedTransform();
        }
        UpdateStats();
    }

    void UpdateTextObjectsUsingUncachedTransform()
    {
        for (var i = 0; i < _iterations; ++i)
        {
            _testObjects[i].RotateUsingNonCachedTransform();
        }
    }

    void UpdateTestObjectsUsingCachedTransform()
    {
        for (var i = 0; i < _iterations; ++i)
        {
            _testObjects[i].RotateUsingCachedTransform();
        }
    }

    public void ToggleCachedTransform()
    {
        ResetStats();
        UseCachedTransform = !UseCachedTransform;
        _useCachedText.text = UseCachedTransform ? "Using Cached Transform" : "Using Non-cached Transform";
        if (UseCachedTransform)
        {
            _cachedFpsText.text = "--";
            _cachedMsText.text = "--";
            _toggleCachedButtonText.text = "Use Uncached";
        }
        else
        {
            _uncachedFpsText.text = "--";
            _uncachedMsText.text = "--";
            _toggleCachedButtonText.text = "Use Cached";
        }
    }

    void ResetStats(bool clearFrameTime = false)
    {
        int size = _fpsArray.Length;
        for (var i = 0; i < size; ++i)
        {
            _fpsArray[i] = 0f;
            _timeArray[i] = 0f;
        }

        _fpsDeltaTime = 0f;
        _counter = 0;
        
        if (!clearFrameTime) return;

        _cachedMs = _uncachedMs = 0;
    }

    void UpdateStats()
    {
        if (!_statsContainer.activeSelf) return;
        _deltaTime = Time.deltaTime;
        _timeArray[_counter] = _deltaTime;
        _fpsDeltaTime += (_deltaTime - _fpsDeltaTime) * 0.1f;
        _fpsArray[_counter] = Mathf.RoundToInt(1.0f / _fpsDeltaTime);
        _totalTime = _totalFps = 0f;
        for (var i = 0; i < _timeArray.Length; ++i)
        {
            _totalTime += _timeArray[i];
            _totalFps += _fpsArray[i];
        }
        _counter = (_counter + 1) % _timeArray.Length;
        
        if (UseCachedTransform)
        {
            _cachedFps = Mathf.RoundToInt(_totalFps / _fpsArray.Length);
            _cachedMs = Mathf.RoundToInt((_totalTime / _timeArray.Length) * 1000f);
            _cachedFpsText.text = $"{_cachedFps}";
            _cachedMsText.text = $"{_cachedMs}ms";
        }
        else
        {
            _uncachedFps = Mathf.RoundToInt(_totalFps / _fpsArray.Length);
            _uncachedMs = Mathf.RoundToInt((_totalTime / _timeArray.Length) * 1000f);
            _uncachedFpsText.text = $"{_uncachedFps}";
            _uncachedMsText.text = $"{_uncachedMs}ms";
        }

        if (_cachedMs == 0 || _uncachedMs == 0)
        {
            _frameTimeDiffText.text = "--";
            return;
        }

        float numerator = (float)Math.Abs(_cachedMs - _uncachedMs);
        float denominator = (float)Mathf.Max(_cachedMs, _uncachedMs);
        float diffPercentage = numerator / denominator;
        _frameTimeDiffText.text = $"{Mathf.RoundToInt(diffPercentage * 100f)}%";
    }

    public void ToggleStats()
    {
        ResetStats();
        _statsContainer.SetActive(!_statsContainer.activeSelf);
    }

    void IterationsSliderChanged(float amount)
    {
        _iterations = (int)_iterationsSlider.value;
        ResetStats(true);
        _iterationsText.text = $"{_iterations}";
        _cachedFpsText.text = "--";
        _cachedMsText.text = "--";
        _uncachedFpsText.text = "--";
        _uncachedMsText.text = "--";
    }
}
