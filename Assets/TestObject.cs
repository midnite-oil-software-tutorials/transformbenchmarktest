using UnityEngine;

public class TestObject : MonoBehaviour
{
    Transform _transform;

    void Awake()
    {
        _transform = transform;
    }

    public void RotateUsingNonCachedTransform()
    {
        transform.position += Vector3.forward;
        transform.Rotate(Vector3.one);
        transform.localScale += Vector3.one;
    }

    public void RotateUsingCachedTransform()
    {
        _transform.position += Vector3.forward;
        _transform.Rotate(Vector3.one);
        _transform.localScale += Vector3.one;
    }
}
